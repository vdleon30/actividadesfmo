<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actividades extends Model
{
    protected $fillable = [
        'solicitud', 'fecha', 'actividad', 'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
