<?php

namespace App\Http\Controllers;

use App\Actividades;
use App\User;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;

class ActividadesController extends Controller
{
    public function registrar()
    {
        return view('registro', ['menu_active' => "registrarActividad"]);
    }

    public function guardar(Request $request)
    {
        $data = $request->only(["solicitud", "fecha", "user_id", "actividad"]);

        $user = Actividades::create([
            'solicitud' => $data['solicitud'],
            'fecha'     => $data['fecha'],
            'actividad' => $data["actividad"],
            'user_id'   => $data["user_id"],
        ]);

        return redirect()->route("actividades.registrar")->with(["status" => "Registrado Exitosamente"]);
    }

    public function consultar()
    {
        if (isset($_GET["form"])) {
            $form = $_GET["form"];
        } else {
            $form = gmdate("Y-m-d");
        }

        if (isset($_GET["till"])) {
            $till = $_GET["till"];
        } else {
            $till = gmdate("Y-m-d");
        }

        $usuarios = User::where("usuario", "<>", "admin")->get();

        $actividades = collect([]);

        if (isset($_GET["user_id"])) {
            $user_id = $_GET["user_id"];

            $actividades = Actividades::where("fecha", ">=", $form)->where("fecha", "<=", $till)->where("user_id", $user_id)->orderBy("fecha", "ASC")->get();
        } else {
            $user_id = null;

        }

        return view('consulta', ["actividades" => $actividades, "user_id" => $user_id, "form" => $form, "till" => $till, 'menu_active' => "consultarActividad", 'usuarios' => $usuarios]);
    }

    public function reportes()
    {

        $usuarios = User::where("usuario", "<>", "admin")->get();

        return view('reportes', ['menu_active' => "consultarReportes", 'usuarios' => $usuarios]);
    }

    public function generarReporte(Request $request)
    {
        if (isset($_GET["form"])) {
            $form = $_GET["form"];
        } else {
            $form = gmdate("Y-m-d");
        }

        if (isset($_GET["till"])) {
            $till = $_GET["till"];
        } else {
            $till = gmdate("Y-m-d");
        }

        $usuarios = User::where("usuario", "<>", "admin")->get();

        $actividades =  Actividades::where("fecha", ">=", $form)->where("fecha", "<=", $till);
        $seccion = "all";
        if (isset($_GET["seccion"]) && $_GET["seccion"] != "all") {
            $seccion     = $_GET["seccion"];
            $actividades = $actividades->whereHas('user', function ($k) use ($seccion){
                        $k->where('seccion',$seccion);
                    });
        } 

        if (isset($_GET["user_id"]) && $_GET["user_id"] != "all") {
            $user_id     = $_GET["user_id"];
            $actividades = $actividades->where("user_id", $user_id)->orderBy("fecha", "ASC")->get();

        } else {
            $user_id     = "all";
            $actividades = $actividades->where("user_id", "<>", "1")->orderBy("fecha", "ASC")->get();
        }

        return \PDF::loadView('pdf', compact("actividades","seccion"))->stream();
    }
}
