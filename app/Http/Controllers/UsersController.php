<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class UsersController extends Controller
{
    public function __construct()
    {
        View::share('menu_active', "usuarios");
    }

    public function usuarios()
    {
        $user = User::getCurrent();
        return view("usuarios.lista", ["breadcrumb" => true]);
    }

    public function listaUsuarios()
    {
        $usuarios = User::where("usuario", "<>", "admin")->get();

        $usuarios_list = [];

        foreach ($usuarios as $usuario) {
            $s = $usuario->seccion ? ucfirst($usuario->seccion) : "No Disponible";
            $usuarios_list[] = [$usuario->email . '<span>' . $usuario->nombre . ' ' . $usuario->apellido . '</span>', ucfirst($usuario->tipo), "{$s}", $usuario->ficha, $usuario->created_at->format("Y/m/d"), '<div "class="container-btn--action-table"><a class="" href="' . route("usuarios.editar", ["id" => $usuario->id]) . '"><i class="fa fa-pencil" aria-hidden="true"></i></a><a id="' . $usuario->id . '" class="text-danger" data-toggle="modal" data-target="#myModal" onclick="confirmar('.$usuario->id.')" data-whatever="aceptar/' . $usuario->id . '"><i class="fa fa-trash" aria-hidden="true"></i></a></div>'];
        }
        return response()->json(['data' => $usuarios_list]);
    }

    public function crear()
    {
        return view("usuarios.crear");
    }

    public function guardar(Request $request)
    {
        $data = $request->only(["nombre", "apellido", "ficha", "seccion", "email", "tipo", "usuario", "password", "password_confirmation"]);

        $validator = Validator::make($data, [
            'nombre'   => ['required', 'string', 'max:255'],
            'apellido' => ['required', 'string', 'max:255'],
            'email'    => ['required', 'string', 'max:255', 'unique:users'],
            'ficha'    => ['required', 'string', 'max:255', 'unique:users'],
            'tipo'     => ['required', 'string', 'max:255'],
            'seccion'  => ['required', 'string', 'max:255'],
            'usuario'  => ['required', 'string', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);

        if ($validator->fails()) {
            return redirect()->route('usuarios.crear')->withErrors($validator)->withInput();
        }

        $user = User::create([
            'nombre'   => ucfirst($data['nombre']),
            'apellido' => ucfirst($data['apellido']),
            'email'    => $data["email"],
            'ficha'    => $data["ficha"],
            'tipo'     => $data["tipo"],
            'usuario'  => $data["usuario"],
            'seccion'  => $data["seccion"],
            "password" => Hash::make($data["password"]),
        ]);

        return redirect()->route("usuarios")->with(["status" => "Usuario Registrado"]);

    }

    public function editar($id)
    {
        $edit_user = User::find($id);
        return view("usuarios.editar", ["user" => $edit_user]);
    }

    public function actualizar(Request $request)
    {
        $data = $request->only(["id","nombre", "apellido", "ficha", "seccion", "email", "tipo", "usuario", "password", "password_confirmation"]);
        $user      = User::find($data["id"]);

        $validator = Validator::make($data, [
            'nombre'   => ['required', 'string', 'max:255'],
            'apellido' => ['required', 'string', 'max:255'],
            'email'    => ['required', 'string', 'max:255', 'unique:users,email,{{$data[id]}}'],
            'ficha'    => ['required', 'string', 'max:255', 'unique:users,tipo,{{$data[id]}}'],
            'tipo'     => ['required', 'string', 'max:255'],
            'seccion'  => ['required', 'string', 'max:255'],
            'usuario'  => ['required', 'string', 'max:255', 'unique:users,usuario,{{$data[id]}}'],
        ]);
        if ($validator->fails()) {
            return redirect()->route('usuarios.editar', $data["id"])->withErrors($validator)->withInput();
        }

        $password = $request->get("password");
        if ($password && !empty($password)) {
            $confirm_password = $request->get("password_confirmation");
            $validator = Validator::make($data, [
                'password' => ['required', 'string', 'min:6', 'confirmed'],
            ]);

            if ($validator->fails()) {
                return redirect()->route('usuarios.editar', $data["id"])->withErrors($validator)->withInput();
            }
            User::where("id", $user->id)->update(["password" => Hash::make($password)]);
        }
        $user->update([
            'nombre'   => ucfirst($data['nombre']),
            'apellido' => ucfirst($data['apellido']),
            'email'    => $data["email"],
            'ficha'    => $data["ficha"],
            'tipo'     => $data["tipo"],
            'usuario'  => $data["usuario"],
            'seccion'  => $data["seccion"],
        ]);
        return redirect()->route("usuarios")->with(["status" => "Usuario actualizado exitosamente"]);
    }

    public function eliminar (Request $request,$id)
    {
        User::find($id)->delete();
        return redirect()->route("usuarios")->with(["status" => "Usuario eliminado exitosamente"]);
    }
}
