<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'apellido', 'tipo', 'ficha', 'email', 'password', 'usuario', 'seccion',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function getCurrent()
    {
        if (\Auth::check()) {
            return \Auth::user();
        }
        return false;
    }

    public function actividades()
    {
        return $this->hasMany('App\Models\Actividades');
    }

    public function nombre_completo()
    {
        return $this->nombre . " " . $this->apellido;
    }

}
