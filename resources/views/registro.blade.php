@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card custom-shadow">
                <div class="card-header text-center">{{ __('Registro') }}</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <form action="{{ route('actividades.guardar') }}" method="POST">
                        @csrf
                        <input type="hidden" name="user_id" value="{{$user->id}}">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="validationDefault01">N° de solicitud</label>
                                <input class="form-control" type="text" name="solicitud" placeholder="N° de solicitud" required="">
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="validationDefault02">Fecha</label>
                                <input class="form-control " type="date" name="fecha" placeholder="Fecha" required="" max='{{gmdate("Y-m-d")}}'>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-12 mb-3">
                                <label for="validationDefault01">Actividades</label>
                                <textarea name="actividad" placeholder="Actividades" class="form-control" rows="6" required></textarea>
                            </div>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-success">Registar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
