@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card custom-shadow">
                <div class="card-header text-center">{{ __('Reportes') }}</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <form action="{{ route('actividades.reportes.generar') }}" method="GET"  target="_blank">
                        <div class="form-row">
                            <div class="col-sm-12 col-md-6 mb-3">
                                <label for="validationDefault01">Usuario</label>
                                <select class="form-control custom-select " name="user_id">
                                    <option value="all">Todos los usuarios</option>
                                    @foreach ($usuarios as $u)
                                        <option value="{{ $u->id }}">{{$u->nombre}} {{$u->apellido}} - {{$u->tipo}} - {{$u->seccion}}</option>
                                    @endforeach
                                </select>
                                
                            </div>
                            <div class="col-sm-12 col-md-6 mb-3">
                                    <label for="validationDefault01">Sección</label>
                                    <select class="form-control custom-select " name="seccion">
                                        <option value="all">Todas las secciones</option>
                                        <option>Apoyo a Usuarios</option>
                                        <option>Soporte Técnico</option>
                                    </select>
                                </div>
                        </div>
                        @csrf
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="validationDefault02">Desde</label>
                                <input class="form-control " type="date" name="form" placeholder="Fecha" max='{{gmdate("Y-m-d")}}' value='{{gmdate("Y-m-d")}}'>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="validationDefault02">Hasta</label>
                                <input class="form-control " type="date" name="till" placeholder="Fecha" max='{{gmdate("Y-m-d")}}' value='{{gmdate("Y-m-d")}}'>
                            </div>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-danger" onclick="">Generar PDF</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
