@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card custom-shadow">
                <div class="card-header text-center">{{ __('Consulas') }}</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <form action="{{ route('actividades.consultar') }}" method="GET">
                        <div class="form-row">
                            <div class="col-12 mb-3">
                                <label for="validationDefault01">Usuario</label>
                                <select class="form-control custom-select " name="user_id">
                                    @foreach ($usuarios as $u)
                                        <option value="{{ $u->id }}" {{$user_id==$u->id?"selected":""}} >{{$u->nombre}} {{$u->apellido}} - {{$u->tipo}} - {{$u->seccion}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @csrf
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="validationDefault02">Desde</label>
                                <input class="form-control " type="date" name="form" placeholder="Fecha" value="{{$form}}" max='{{gmdate("Y-m-d")}}'>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="validationDefault02">Hasta</label>
                                <input class="form-control " type="date" name="till" placeholder="Fecha" value="{{$till}}" max='{{gmdate("Y-m-d")}}'>
                            </div>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-success" onclick="">Consultar</button>
                        </div>
                        @if ($actividades->isNotEmpty())
                            <hr>
                            <h3 class="text-muted">Actividades:</h3>
                            @foreach ($actividades as $element)
                                <div class="card mb-3 custom-shadow">
                                    <h5 class="card-header">Solicitud N° {{$element->solicitud}}</h5>
                                    <div class="card-body">
                                        <h5 class="card-title">Fecha: {{$element->fecha}}</h5>
                                        <p class="card-text">{!!nl2br($element->actividad)!!}</p>
                                    </div>
                                </div>
                            @endforeach
                        @elseif($user_id)
                            <hr>
                            <h3 class="text-muted">Actividades:</h3>
                            <div class="card mb-3 custom-shadow">
                                <div class="card-body text-center">
                                    <h5 class="card-title">Usuario no posee actividades en las fechas consultadas</h5>
                                </div>
                            </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
