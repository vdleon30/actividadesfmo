    @extends('layouts.app')

    @section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-md-10 col-lg-8">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <div class="card shadow mb-4">
                    <div class="card-header text-center inline">
                        <span>{{ __('Usuarios') }}
                            <a href="{{ route('usuarios.crear') }}" class="btn btn-primary " style="float: right;position: absolute;right: 13px;top: 5px;">Registar</a>
                        </span>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped" id="dataTable" width="100%" cellspacing="0" data-content="Usuarios" data-url="{{route("usuarios.lista")}}">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Tipo</th>
                                        <th>Sección</th>
                                        <th>Ficha</th>
                                        <th>Registrado</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    @endsection

