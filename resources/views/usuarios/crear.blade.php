@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card custom-shadow">
                <div class="card-header text-center">{{ __('Registrar Usuario') }}</div>
                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <form method="post" action="{{ route('usuarios.guardar') }}">
                        @csrf
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="validationDefault01">Nombre</label>
                                <input class="form-control {{ $errors->has('nombre') ? ' is-invalid' : '' }}" type="text" name="nombre" placeholder="Nombre" required value="{{ old("nombre")}}">
                                @if ($errors->has('nombre'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('nombre') }}</strong>
                                </span>
                                @endif                                    
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="validationDefault02">Apellido</label>
                                <input class="form-control {{ $errors->has('apellido') ? ' is-invalid' : '' }}" type="text" name="apellido" placeholder="Apellido" required value="{{ old("apellido")}}">
                                @if ($errors->has('apellido'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('apellido') }}</strong>
                                </span>
                                @endif                                    
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="validationDefault01">Correo Electrónico</label>
                                <input class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" type="email" name="email" placeholder="Correo Electrónico" required value="{{ old("email")}}">
                                @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif                                    
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="validationDefault02">Ficha</label>
                                <input class="form-control {{ $errors->has('ficha') ? ' is-invalid' : '' }}" type="text" name="ficha" placeholder="Ficha" required value="{{ old("ficha")}}">
                                @if ($errors->has('ficha'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('ficha') }}</strong>
                                </span>
                                @endif                                    
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="validationDefault02">Usuario</label>
                                <input class="form-control {{ $errors->has('usuario') ? ' is-invalid' : '' }}" type="text" name="usuario" placeholder="Usuario" required value="{{ old("usuario")}}">
                                @if ($errors->has('usuario'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('usuario') }}</strong>
                                </span>
                                @endif                                    
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="validationDefault01">Tipo de Usuario</label>
                                <select class="form-control {{ $errors->has('tipo') ? ' is-invalid' : '' }} custom-select " name="tipo">
                                    <option {{ old('tipo') == "Jefe"?"selected":"" }}>Jefe</option>
                                    <option {{ old('tipo') == "Analista"?"selected":"" }}>Analista</option>
                                </select>
                                @if ($errors->has('tipo'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('tipo') }}</strong>
                                </span>
                                @endif                                    
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="validationDefault01">Sección</label>
                                <select class="form-control {{ $errors->has('seccion') ? ' is-invalid' : '' }} custom-select " name="seccion">
                                    <option {{ old('seccion') == "Apoyo a Usuarios"?"selected":"" }}>Apoyo a Usuarios</option>
                                    <option {{ old('seccion') == "Soporte Técnico"?"selected":"" }}>Soporte Técnico</option>
                                </select>
                                @if ($errors->has('seccion'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('seccion') }}</strong>
                                </span>
                                @endif                                    
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-xs-12 col-sm-6">
                                <label class="required" for="password">{{ __('Contraseña') }}</label>
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Contraseña" required>
                                @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif                                    
                            </div>
                            <div class="form-group col-xs-12 col-sm-6">
                                <label class="required" for="password-confirm">{{ __('Confirmar Contraseña') }}</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirmar Contraseña" required>
                                @if ($errors->has('password_confirmation'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                                @endif                                    
                            </div>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-success">Registar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
