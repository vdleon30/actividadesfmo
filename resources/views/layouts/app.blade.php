<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SIRECAD</title>
    

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href="{{ asset('img/logoferrominera.ico') }}">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body >
    <style type="text/css">
        .custom-shadow {
            box-shadow: 0px 5px 13px -8px black !important;
        }
    </style>
    <script type="text/javascript">
        function confirmar(e) {
            var r = confirm("¿Seguro que desea eliminar el usuario?");
            if (r == true) {
                window.open('/usuarios/eliminar/'+e, '_self');
            }
        }
    </script>
    <div id="">
        <nav class="navbar navbar-expand-lg navbar-light bg-light bg-white shadow-sm">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}">Registro de Actividades</a>

            <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                    
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Iniciar Sesión') }}</a>
                        </li>
                    @else
                    <li class="nav-item">
                        <a class="nav-link {{isset($menu_active) && $menu_active == "home"?"active":""}}"  href="{{ route('home') }}">Home</a>
                    </li>
                    @if (strtolower($user->tipo) == "jefe" || strtolower($user->tipo) == "admin" )
                        <li class="nav-item  ">
                            <a class="nav-link {{isset($menu_active) && $menu_active == "usuarios"?"active":""}}" href="{{ route('usuarios') }}" >Usuarios</a>
                        </li>
                    @endif
                    @if (strtolower($user->tipo) != "admin" )
                    <li class="nav-item  ">
                        <a class="nav-link {{isset($menu_active) && $menu_active == "registrarActividad"?"active":""}}" href="{{ route('actividades.registrar') }}" >Registro</a>
                    </li>
                    @endif
                    @if (strtolower($user->tipo) != "analista")
                    <li class="nav-item">
                        <a class="nav-link {{isset($menu_active) && $menu_active == "consultarActividad"?"active":""}}" href="{{ route('actividades.consultar') }}" tabindex="-1" aria-disabled="true">Consulta</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{isset($menu_active) && $menu_active == "consultarReportes"?"active":""}}" href="{{ route('actividades.reportes') }}" tabindex="-1" aria-disabled="true">Reportes</a>
                    </li>
                    @endif
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->nombre }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Cerrar Sesión') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>

    </div>

</body>
</html>
