<!DOCTYPE html>
<html>
<head>
	<title>Reporte</title>
</head>
<body>

	<style>
		.page-break {
			page-break-after: always;
		}
		table, th, td {
			border: 1px solid black;
			border-collapse: collapse;
			font-size: 12px
		}
		thead td{
			text-align: center;
			font-weight: 900
		}
		td,th{
			padding: .3rem .5rem
		}
	</style>
	 
     <img height="80" class="" style="position: absolute;" src="{{public_path("img/logo.png")}}">

	<div style="width: 100%;text-align: center;margin-top: 3rem">
		<h6 style="margin-bottom:  0">ACTIVIDADES REALIZADAS</h6>
		@if ($seccion != "all")
			<h6 style="margin-top:  1rem;text-transform: uppercase;">SECCIÓN {{ strtoupper($seccion) }}</h6>
			{{-- true expr --}}
		@else
			<h6 style="margin-top:  1rem">TODAS LAS SECCIONES</h6>
			{{-- false expr --}}
		@endif
	</div>
	<div style="width: 100%; padding: 0 60px">
		<table style="width: 100%">
			<thead>
				<tr>
					<th>FECHA</th>
					<th>ANALISTA</th>
					<th>ACTIVIDADES</th>
					<th>PENDIENTE</th>
					<th>OBSERVACION</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($actividades as $element)
				<tr>
					<td style="text-align: center;">{!! \DateTime::createFromFormat("Y-m-d",$element->fecha)->format("d/m/Y")!!}</td>
					<td style="text-align: center;">{{$element->user->nombre_completo()}} <br><br> F-{{$element->user->ficha}}</td>
					<td>Solicitud {{$element->solicitud}} <br> <p>{{$element->actividad}}</p></td>
					<td></td>
					<td></td>
				</tr>
				@endforeach
				@if ($actividades->isEmpty())
					<tr>
						<td style="text-align: center;font-size: 12px;padding: 1.3rem" colspan="5" >No hay datos disponibles.</td>
					</tr>
				@endif
			</tbody>
		</table>
	</div>
</body>
</html>
