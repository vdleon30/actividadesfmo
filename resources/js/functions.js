$(function() {
    "use strict";
    $(document).on("init.dt", function(e, settings) {
        $('[data-toggle="tooltip"]').tooltip();
        $(".workingThis").tooltip({
            title: "Estamos trabajando en esto 😊",  
            html: true
        });
    });
    
    var targets = $("#dataTable").data("targets");
    if (targets === undefined) {
        targets = -1;
    };
    $("#dataTable").DataTable({
        ajax: $("#dataTable").data("url"),
        columnDefs: [{ orderable: false, targets: targets }],
        iDisplayLength: 50,
        language: {
            lengthMenu: "Mostrar _MENU_ " + $("#dataTable").data("content"),
            info:
                "Mostrando _START_ a _END_ de _TOTAL_ " +
                $("#dataTable").data("content"),
            infoEmpty:
                "Mostrando 0 a 0 de 0 " + $("#dataTable").data("content"),
            search: "Buscar:",
            emptyTable: "No hay datos disponibles",
            loadingRecords: "Cargando...",
            processing: "Procesando...",
            paginate: {
                first: "First",
                last: "Last",
                next: "Siguente",
                previous: "Anterior"
            }
        }
    });
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
});
