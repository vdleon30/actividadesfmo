<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
   Auth::routes();

Route::group(["middleware" => "auth","middleware" => "logged"], function () { ///Al solicitar pagina web que este iniciada sesion
    Route::get('/', function () {
        return view('welcome');
    });


    Route::get('/home', 'HomeController@index')->name('home');


    //usuarios
    Route::get('/usuarios', 'UsersController@usuarios')->name('usuarios');
    Route::get('/usuarios/lista', 'UsersController@listaUsuarios')->name('usuarios.lista');
    Route::get('/usuarios/crear', 'UsersController@crear')->name('usuarios.crear');
    Route::post('/usuarios/crear', 'UsersController@guardar')->name('usuarios.guardar');
    Route::get('/usuarios/editar/{id}', 'UsersController@editar')->name('usuarios.editar');
    Route::post('/usuarios/editar', 'UsersController@actualizar')->name('usuarios.actualizar');
    Route::get('/usuarios/eliminar/{id}', 'UsersController@eliminar')->name('usuarios.eliminar');

    Route::get('/registro', 'ActividadesController@registrar')->name('actividades.registrar');
    Route::post('/registro/guardar', 'ActividadesController@guardar')->name('actividades.guardar');
    Route::get('/consultas', 'ActividadesController@consultar')->name('actividades.consultar');
    Route::get('/reportes', 'ActividadesController@reportes')->name('actividades.reportes');
    Route::get('/reportes/generar', 'ActividadesController@generarReporte')->name('actividades.reportes.generar');
});
