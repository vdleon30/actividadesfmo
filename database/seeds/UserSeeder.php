<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = ['users' => [

            ['nombre'    => 'Administrador',
                'apellido'   => 'Administrador',
                'email'      => 'admin@admin.com',
                'tipo'       => 'admin',
                'usuario'    => 'admin',
                'password'   => Hash::make("admin1234"),
                'created_at' => gmdate("Y-m-d H:i:s"),
                'updated_at' => gmdate("Y-m-d H:i:s"),
            ],
        ]];

        // Insertar datos en la BD

        foreach ($data['users'] as $users) {
            // Inserta los categorias en la BD

            DB::table('users')->insert([
                'nombre'     => $users['nombre'],
                'apellido'   => $users['apellido'],
                'email'      => $users['email'],
                'tipo'       => $users['tipo'],
                'usuario'    => $users['usuario'],
                'password'   => $users['password'],
                'created_at' => $users['created_at'],
                'updated_at' => $users['updated_at'],
            ]);
        }
    }
}
